module.exports = {
    ApiUserRouter: require('./user/apiRoutes'),
    UserRouter: require('./user/interfaceRoutes'),
    IndexRouter: require('./index'),
    ApiHacker: require('./hackerNews/apiRoutes'),
}