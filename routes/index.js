const express = require('express');
const app = express.Router();
const passport = require('passport');
const Auth = require('../controllers/auth');
const Mid = require('../middlewares/index');

app.get('/', function(req, res, next) {
  res.render('home', { title: 'Express', layout: 'layouts/blank' });
});
app.get('/main', Mid.isLoggedIn, function(req, res, next) {
  res.render('main/index', { title: 'Express' });
});
app.post('/signup', passport.authenticate('local-signup', {
  successRedirect: '/login', // redirect to the secure login section
  failureRedirect: '/signup', // redirect back to the signup page if there is an error
  failureFlash: true // allow flash messages
}));
app.post('/login', passport.authenticate('local-login', {
  successRedirect: '/main', // redirect to the secure main section
  failureRedirect: '/login', // redirect back to the login page if there is an error
  failureFlash: true // allow flash messages
}));
app.get('/login', Mid.alreadyLoggedIn, Auth.renderLogIn);
app.get('/logout',Auth.logOut);
app.get('/signup', Auth.renderSignUp);
 //============= reset pass ================
app.get('/forgotpass',  Auth.renderForgotPass);
app.get('/reset/:token', Auth.receiveValidToken);
app.post('/send-email', Auth.validateEmail);
app.post('/updatepass/:token', Auth.resetPass);

app.get('/failsafe',  function(req, res) {
    res.status(200).send('Funciona correctamente')
})
module.exports = app;
