/**********************************************/
/****************PACKAGES**********************/
/**********************************************/
const express = require('express')
const router = express.Router();
const Mid = require('../../middlewares/index');
/**********************************************/
/****************CONTROLLER********************/
/**********************************************/
const HackerNewsController = require('../../controllers/hackerNews');
/*************************************/
/*************** FUNCTIONS ***********/
/*************************************/
router.get('/news', HackerNewsController.findNews);
router.post('/articles', HackerNewsController.createArticles);
router.get('/articles', HackerNewsController.findArticles);
router.get('/article/:id', HackerNewsController.findArticle);
router.delete('/articles', HackerNewsController.deleteArticles);
router.delete('/article/:id', HackerNewsController.deleteArticle);
router.put('/article/:id', HackerNewsController.updateArticle);
router.get('/articles-pag', HackerNewsController.findAllPaginated);
router.get('/articles-author/:author', HackerNewsController.findByAuthor);
router.get('/articles-title/:title', HackerNewsController.findByTitle);
router.get('/articles-tags', HackerNewsController.findByTags);
router.get('/articles-query-pag', HackerNewsController.findArticlesByQueryByPag);
/*****************END*****************/
module.exports = router;