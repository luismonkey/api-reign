## ROUTES

- Esta carpeta se utiliza para el almacenamiento de los archivos que contienen las rutas de los componentes, separando la API (fuente de informacion) y las Vistas (render de las vistas).
- index.js
    - login
    - sigup
    - logout
    - routings.js: Este Archivo contiene la centralización de las rutas creadas que serán llamadas dentro de app.js o cronworker.js para su respectivo uso.

* Paso 1: Carpeta con el nombre componente (y/o modelo, depende de los controladores), con el formato 'lowercase'
* Paso 2: Archivo `interfaceRoutes.js` y `apiRoutes.js` , con el formato 'camelCase'

- Ejemplo
    - /user:    
        - interfaceRoutes.js
        - apiRoutes.js