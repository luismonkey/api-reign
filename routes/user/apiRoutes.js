/**********************************************/
/****************PACKAGES**********************/
/**********************************************/
const express = require('express')
const router = express.Router();
const Mid = require('../../middlewares/index');
/**********************************************/
/****************CONTROLLER********************/
/**********************************************/
const UserController = require('../../controllers/user')
/*************************************/
/*************** FUNCTIONS ***********/
/*************************************/
router.get('/saludo', UserController.saludo);
/*****************END*****************/
module.exports = router;
