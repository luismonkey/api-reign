require('dotenv').config()
const createError = require('http-errors');
const express = require('express');
const expressLayouts   = require('express-ejs-layouts');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const db = require('./config/db');
const passport       = require('passport');
const session        = require('express-session');
const flash          = require('connect-flash');
const MongoStore   = require('connect-mongo')
const port             = process.env.PORT || 8086;
//==========CONFIGURACION=================
db.connection(); //dataBase Connection
require('./services/passport')(passport); // pass passport for configuration
const {
  ApiUserRouter, IndexRouter, UserRouter, ApiHacker,
} = require('./routes/routings');

const app = express();
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(logger('dev',{ skip: function(req, res) { return res.statusCode == 304 }}));
app.use(expressLayouts);
app.set("layout extractScripts", true);
app.set("layout extractStyles", true);
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
//====== required for passport======================
app.use(session({
  secret: process.env.SECRET_KEY_SESSION,
  resave: true,
  saveUninitialized: true,
  store: MongoStore.create({ mongoUrl: process.env.NODE_ENV === 'production' ? process.env.DATA_BASE_PROD : process.env.DATA_BASE }),
}));
app.use(passport.initialize());
//initializes the passport configuration
app.use(passport.session());
app.use(flash());



app.use('/', IndexRouter)
app.use('/apiuser', ApiUserRouter);
app.use('/user', UserRouter);
app.use('/apiHacker', ApiHacker)

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  console.log('handle error...', err)
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  // render the error page
  res.status(err.status || 500);
  res.render('error/error', {layaout: 'layaouts/blank', error: err});
});

app.use(logger('dev', {
  skip: function (req, res) { return res.statusCode == 304 }
}))

if(process.env.NODE_ENV === 'production') {
  console.log("production enviroment");
  app.use(logger(':date[iso] :remote-addr :remote-user :method :url HTTP/:http-version :status :res[content-length] - :response-time ms', { stream: accessLogStream, skip: function (req, res) { return res.statusCode == 304 }}))
  global.url = 'http://google.com/'; //CHANGE HERE THE URL OF PRODUCTION
} else {
  process.env.NODE_ENV = "development"
  console.log("development enviroment");
  app.use(logger('dev', {
    skip: function (req, res) { return res.statusCode == 304 }
  }))
  global.url = 'http://localhost:'+port+'/';
}
app.listen(port);
console.log(`DASHBOARD TEMPLATE RUN IN: ${global.url}`);
module.exports = app;
