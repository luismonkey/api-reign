const mongoose = require('mongoose');

const articleSchema = mongoose.Schema({
    title                : String,
    url                  : String,
    author               : String,
    points               : String,
    story_text           : String,
    comment_text         : String,
    num_comments         : String,
    story_id             : String,
    story_title          : String,
    story_url            : String,
    parent_id            : String,
    created_at_i         : String,
    _tags                : Array,
    objectID             : String,
    _highlightResult     : Object,
    
});

module.exports = mongoose.model('Article', articleSchema);