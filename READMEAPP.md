## APP.JS

- Contiene las configuraciones para el funcionamiento de la aplicación incluyendo la estructura de las rutas. 
- Formato: camelCase , usar el nombre de la aplicación


## CRONWORKER.JS

- Este archivo está diseñado para separar los procesos de Nodejs de el Dashboard/api de la aplicación y no interrumpir estos procesos, y realizar el procesamiento de información en otro proceso, para mejorar el rendimiento de las aplicaciones en general.
- Aquí llamamos los controladores que manejan las funciones de procesamiento en “segundo plano” 
- Formato: camelCase 


