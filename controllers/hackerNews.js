/**********************************************/
/****************PACKAGES**********************/
/**********************************************/
/**********************************************/
/****************UTILS*************************/
/**********************************************/
/***********************************************/
/****************MODELS*************************/
/**********************************************/
const ModelsMongoose = require("../services/mongooseModelService");
const ArticlesModel = new ModelsMongoose("articles");
/***********************************************/
/****************SERVICES***********************/
/**********************************************/
const ServiceHackeNews = require('../services/hackerNews');
/*************************************/
/*************** FUNCTIONS ***********/
/*************************************/
const controller = {}
//***************** API ******************* */

/* this is a function that queries data from Hacker News */
controller.findNews = async () => {
    return new Promise(async function (resolve) {
        const response = await ServiceHackeNews.getData()
        if(response){
            const data = response;
            resolve(data.hits)
        }else{
            resolve(null)
        }
    });
};

/* This function is called by the cron job, which is responsible for querying the data from Hacker News
and saving it in the database. */
controller.createArticlesByCron = async () => {
    const response = await controller.createArticles()
};

/* A function that queries data from Hacker News and saves it in the database. */
controller.createArticles = async () => {
    return new Promise(async function (resolve) {
        const response = await controller.findNews();
        let obj = {}
        let articles_created = [];
        for await(const a of response){
            obj = {
                title                : a.title,
                url                  : a.url,
                author               : a.author,
                points               : a.points,
                story_text           : a.story_text,
                comment_text         : a.comment_text,
                num_comments         : a.num_comments,
                story_id             : a.story_id,
                story_title          : a.story_title,
                story_url            : a.story_url,
                parent_id            : a.parent_id,
                created_at_i         : a.created_at_i,
                _tags                : a._tags,
                objectID             : a.objectID,
                _highlightResult     : a._highlightResult,
            }
            await ArticlesModel.findOne({objectID: a.objectID.toString()}).then(async article => {
                if(article.body === null){
                    await ArticlesModel.create(obj).then(created => {
                        if(created.body){
                            articles_created.push(created.body)
                            console.log('Article created successfully!!')
                        }
                    })
                }else{
                    console.log('The article exist!!')
                }
            })
        }
        resolve(articles_created)
    });
};

/* A function that queries all the articles from the database. */
controller.findArticles = (req, res, next) => {
    try {
        ArticlesModel.findMany({}).then(articles => {
            res.json({
                ok: 1,
                msg: 'Successful',
                data: articles.body,
                err: null
            })
        })
    } catch (error) {
        res.json({
            ok: 0,
            msg: 'An error occurred, please try again',
            data: null,
            err: error
        })
    }
};

/* This function is responsible for querying an article from the database. */
controller.findArticle = (req, res, next) => {
    try {
        const id = req.params.id;
        ArticlesModel.findOne({_id: id}).then(article => {
            if(article.body){
                res.json({
                    ok: 1,
                    msg: 'Successful',
                    data: article.body,
                    err: null
                })
            }else{
                res.json({
                    ok: 1,
                    msg: 'Article not found',
                    data: null,
                    err: null
                })
            }
        })
    } catch (error) {
        res.json({
            ok: 0,
            msg: 'An error occurred, please try again',
            data: null,
            err: error
        })
    }
};

/* Deleting all the articles from the database. */
controller.deleteArticles = (req, res, next) => {
    try {
        ArticlesModel.findMany({}).then(async articles => {
            if(articles.body){
                for await(const a of articles.body){
                    ArticlesModel.delete(a._id).then(deleted => {
                        console.log('DELETED!!')
                    })
                }
            }
        })
    } catch (error) {
        res.json({
            ok: 0,
            msg: 'An error occurred, please try again',
            data: null,
            err: error
        })
    }
};

/* A function that deletes an article from the database. */
controller.deleteArticle = (req, res, next) => {
    try {
        const id = req.params.id;
        ArticlesModel.findOne({_id: id}).then(article => {
            if(article.body){
                const _id = article.body._id;
                ArticlesModel.delete(_id).then(article_deleted => {
                    if(article_deleted.body){
                        res.json({   
                            ok: 1,
                            msg: 'Article deleted',
                            data: article_deleted.body,
                            err: null
                        })
                    }else {
                        res.json({
                            ok: 0,
                            msg: 'An error occurred while deleting, please try again',
                            data: null,
                            err: 'Error'
                        })
                    }
                })
            }else{
                res.json({
                    ok: 1,
                    msg: 'Article not found',
                    data: null,
                    err: null
                })
            }
        })
    } catch (error) {
        res.json({
            ok: 0,
            msg: 'An error occurred, please try again',
            data: null,
            err: error
        })
    }
};

/* This function is responsible for updating an article in the database. */
controller.updateArticle = (req, res, next) => {
    try {
        const id = req.params.id;
        let obj = req.body;
        ArticlesModel.findOne({_id: id}).then(article => {
            if(article.body){
                const _id = article.body._id;
                ArticlesModel.updateOne(_id, obj).then(article_updated => {
                    if(article_updated.body){
                        res.json({
                            ok: 1,
                            msg: 'Article updates',
                            data: article_updated.body,
                            err: null
                        })
                    }else{
                        res.json({
                            ok: 0,
                            msg: 'An error occurred while updating, please try again',
                            data: null,
                            err: 'Error'
                        })
                    }
                })
            }else{
                res.json({
                    ok: 1,
                    msg: 'Article not found',
                    data: null,
                    err: null
                })
            }
        })
    } catch (error) {
        res.json({
            ok: 0,
            msg: 'An error occurred, please try again',
            data: null,
            err: error
        })
    }
};

/* A pagination function that is used to paginate the articles. */
controller.findAllPaginated = (req, res, next) => {
    try {
        const perPage = 5 // req.query.perPage; 
        const page = req.query.page;
        ArticlesModel.findPagination({},perPage,page).then(articles => {
            res.json({
                ok: 1,
                msg: 'Successful',
                data: articles.body,
                err: null
            })
        })
    } catch (error) {
        res.json({
            ok: 0,
            msg: 'An error occurred, please try again',
            data: null,
            err: error
        })
    }
}

/* The above code is a function that is used to find articles by author. */
controller.findByAuthor = (req, res, next) => {
    try {
        let author = req.params.author;
        ArticlesModel.findMany({author: author}).then(articles => {
            if(articles.body.length > 0){
                res.json({
                    ok: 1,
                    msg: 'Successful',
                    data: articles.body,
                    err: null
                })
            }else{
                res.json({
                    ok: 1,
                    msg: 'Article not found',
                    data: null,
                    err: null
                })
            }
        })
    } catch (error) {
        res.json({
            ok: 0,
            msg: 'An error occurred, please try again',
            data: null,
            err: error
        })
    }
};

/* The above code is a function that is used to find articles by title. */
controller.findByTitle = (req, res, next) => {
    try {
        let title = req.params.title;
        ArticlesModel.findMany({title: title}).then(articles => {
            if(articles.body.length > 0){
                res.json({
                    ok: 1,
                    msg: 'Successful',
                    data: articles.body,
                    err: null
                })
            }else{
                res.json({
                    ok: 1,
                    msg: 'Article not found',
                    data: null,
                    err: null
                })
            }
        })
    } catch (error) {
        res.json({
            ok: 0,
            msg: 'An error occurred, please try again',
            data: null,
            err: error
        })
    }
};

/* The above code is a function that is used to find articles by tags. */
controller.findByTags = (req, res, next) => {
    try {
        let tags_query = req.query.tags;
        let tags = tags_query.split(',');
        ArticlesModel.findMany({_tags: { $in: tags }}).then(articles => {
            if(articles.body.length > 0){
                res.json({
                    ok: 1,
                    msg: 'Successful',
                    data: articles.body,
                    err: null
                })
            }else{
                res.json({
                    ok: 1,
                    msg: 'Article not found',
                    data: null,
                    err: null
                })
            }
        })
    } catch (error) {
        res.json({
            ok: 0,
            msg: 'An error occurred, please try again',
            data: null,
            err: error
        })
    }
};

/* A function that is called when a user makes a request to the server. */
controller.findArticlesByQueryByPag = (req, res, next) => {
    try {
        const author = req.query.author ? req.query.author : '';
        const title = req.query.title ? req.query.title : '';
        let tags_query = req.query.tags;
        let tags = tags_query ? tags_query.split(',') : [];
        const perPage = 5 // req.query.perPage; 
        const page = req.query.page ? req.query.page : 0;
        let query_array = [];
        if(author != ''){
            query_array.push({author: author})
        }
        if(title != ''){
            query_array.push({title: title})
        }
        if(tags.length > 0){
            query_array.push({_tags: { $in: tags }})
        }
        let query = query_array.length > 0 ? {$and: query_array} : {};
        ArticlesModel.findPagination(query, perPage, page).then(articles => {
            res.json(articles.body)
        })
    } catch (error) {
        res.json({
            ok: 0,
            msg: 'An error occurred, please try again',
            data: null,
            err: error
        })
    }
};

/*****************END*****************/
module.exports = controller;