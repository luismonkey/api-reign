//=====================middleware============================
module.exports = {
    isLoggedIn: function(req, res, next) {
      if (req.isAuthenticated()){
        return next();
      }else{
        return res.redirect('/login');
      }
    },
    alreadyLoggedIn: function(req, res, next) {
      if (req.isAuthenticated()){
        return res.redirect('/main');
      }else{
        return next();
      }
    },
};
  