const MongooseService = require("./mongooseService"); // Data Access Layer

class ModelService {
  /**
   * @description Create an instance of TransactionService
   */
  constructor(model) {
      const MODEL = require('../models/'+model);
      this.MongooseServiceInstance = new MongooseService(MODEL);
    // Create instance of Data Access layer using our desired mode
  }
  //create
  /**
   * @description Attempt to create a model with the provided object
   * @param modelToCreate {object} Object containing all required fields to
   * create credite Note
   * @returns {Promise<{success: boolean, error: *}|{success: boolean, body: *}>}
   */
  async create(modelToCreate) {
    try {
      const result = await this.MongooseServiceInstance.create(modelToCreate);
      return { success: true, body: result };
    } catch (err) {
      throw err;
    }
  }
  /**
   * @description Attempt to find Many credit notes with the provided query
   * @param query {object} Query to be performed on the Model
   * @returns {Promise<{success: boolean, error: *}|{success: boolean, body: *}>}
   */
  async findMany (query, projection = null, sort = null, options = { lean: false }) {
    try {
        const result = await this.MongooseServiceInstance.find(query, projection, sort, options);
        return { success: true, body: result };
    } catch (err) {
        throw err;
    }
  };
  /**
   * @description Attempt to find Many model with the provided query
   * @param id {string} Required: ID for the object to retrieve
   * @returns {Promise<{success: boolean, error: *}|{success: boolean, body: *}>}
   */
  async findOneById(id) {
    try {
        const result = await this.MongooseServiceInstance.findById(id);
        return { success: true, body: result };
    } catch (err) {
        throw err;
    }
  }
  /**
   * @description Attempt to find One with the provided query
   * @param query  for the object to retrieve
   * @returns {Promise<{success: boolean, error: *}|{success: boolean, body: *}>}
   */
  async findOne(query) {
    try {
      const result = await this.MongooseServiceInstance.findOne(query);
      return { success: true, body: result };
    } catch (err) {
      return { success: false, error: err };
    }
  }
   /**
   * @description Attempt to find One with the provided query
   * @param query  for the object to retrieve
   * @returns {Promise<{success: boolean, error: *}|{success: boolean, body: *}>}
   */
    async findPagination(query, perPage, page) {
      try {
        const result = await this.MongooseServiceInstance.pagination(query, perPage, page);
        return { success: true, body: result };
      } catch (err) {
        return { success: false, error: err };
      }
    }
  /**
   * @description Attempt to edit One model with the provided id
   * @param {String} id Required: ID for the object to retrieve
   * @param {Object} modelToUpdate Object containing all required fields to
   * @returns {Promise<{success: boolean, error: *}|{success: boolean, body: *}>}
   */
  async updateOne(id, modelToUpdate) {
    try {
      const query = {_id: id}
      const result = await this.MongooseServiceInstance.updateOne(
        query,
        modelToUpdate
      );
      return { success: true, body: result };
    } catch (err) {
      throw err;
    }
  }
  /**
   * @description Attempt to edit One model with the provided id
   * @param {String} id Required: ID for the object to retrieve
   * @param {Object} modelToUpdate Object containing all required fields to
   * @returns {Promise<{success: boolean, error: *}|{success: boolean, body: *}>}
   */
  async updateMany(query, modelToUpdate) {
    try {
      const result = await this.MongooseServiceInstance.updateMany(
        query,
        modelToUpdate
      );
      return { success: true, body: result };
    } catch (err) {
      throw err;
    }
  }
  
  /**
   * @description Attempt to logical delete One credit note with the provided id
   * @param {String} id  Required: ID for the object to retrieve
   * @returns {Promise<{success: boolean, error: *}|{success: boolean, body: *}>}
   */
  async logicaDeleteOne(id) {
    try {
      const result = await this.MongooseServiceInstance.update(id, {
        deleted: true,
      });
      return { success: true, body: result };
    } catch (err) {
      throw err;
    }
  }
  /**
   * @description Attempt to physically delete One credit note with the provided id
   * @param {*} id
   * @returns {Promise<{success: boolean, error: *}|{success: boolean, body: *}>}
   */
  async delete(id) {
    try {
      const result = await this.MongooseServiceInstance.delete(id);
      return { success: true, body: result };
    } catch (err) {
      throw err;
    }
  }
   /**
   * @description Attempt to get array data with group diferent
   * @param {String} id Required: ID for the object to retrieve
   * @param {Object} creditNoteToUpdate Object containing all required fields to
   * @returns {Promise<{success: boolean, error: *}|{success: boolean, body: *}>}
   */

    async transformDataByAggregate(pipeline) {
      try {
        const result = await this.MongooseServiceInstance.aggregate(pipeline);
        return { success: true, body: result };
      } catch (err) {
        throw err;
      }
    }
}

module.exports = ModelService;