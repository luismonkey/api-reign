## SERVICES

- Esta carpeta se utiliza para el almacenamiento de los archivos que contienen la logica de negocio de la aplicacion, separado por componente.
- El servicio es responsable de hacer el trabajo y devolverlo al controlador.
- Estos archivos seran llamados por el controlador correspondiente
- Formato camelCase, y el mismo nombre del componente