
/*************** MODELS ***********/
const User = require('../models/user');

const service = {}
/**
 * @param {Object} userObj 
 * @returns create user
 */
 service.createOne = (userObj)=>{
    return new Promise((resolve, reject)=> {
        User.findOne({$and: [{"local.email": userObj.email}, {userName: userObj.userName}]}).then((document) => {
            if(!document){
                User.create(userObj, function (err, data) {
                    if(err) reject(err)
                    return resolve(data)
                });
            }else{
                const err = new Error('Already exist!')
                return reject(err)
            }  
        }).catch((err)=>{
           return reject(err)
        });
    });
};
/**
 * 
 * @param {String} idObj 
 * @returns user
 */
 service.getOne = (idObj)=>{
    return new Promise((resolve, reject)=>{
        User.findOne({_id: idObj}).then((document)=>{
            if(!document){
                const err = new Error("Not found")
                return reject(err)
            }
            return resolve(document)
        }).catch((err)=>{
            return reject(err)
        });
    });
};
/**
 * 
 * @returns users
 */
 service.getAll = ()=>{
       return new Promise((resolve, reject)=>{
            User.find({}).then((documents)=>{
                return resolve(documents)
            }).catch((err)=>{
                return reject(err)
            })
       });
};
/**
 * 
 * @param {String} idObj 
 * @param {Object} updateDocument 
 * @returns user updated
 */
 service.updateOne = (match, updateDocument, options)=>{
    return new Promise((resolve, reject)=>{
        User.findOneAndUpdate(match, updateDocument, options).then((document)=>{
            return resolve(document)
        }).catch((err)=>{
            return reject(err)
        });
    });
};

service.saludo = () => {
return new Promise(async function(resolve, reject) {
    try {
        resolve("Programa como si la persona que mantendrá tú código fuera un psicópata que sabe donde vives :) (Martin Golding)")
    } catch (error) {
        reject(error)
    }
});
}
/*****************END*****************/
module.exports = service