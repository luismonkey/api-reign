/**********************************************/
/****************PACKAGES**********************/
/**********************************************/
const request = require("request");
require("dotenv").config();
/*************************************/
/*************** FUNCTIONS ***********/
/*************************************/
const Service = {};
const URL = 'https://hn.algolia.com/api/v1/search_by_date?query=nodejs';
let HEADER = {}
Service.getData = () => {
    return new Promise(function (resolve) {
        HEADER = {
            'Accept': 'application/json'
        }
        const options = {
            url: URL,
            headers: HEADER
        };
        request(options, function (error, response) {
            if (error) {
                resolve({err: error});
            } else {
                let obj = {}
                obj = JSON.parse(response.body)
                resolve(obj);
            }
        });
    });
};

/*****************END*****************/
module.exports = Service;