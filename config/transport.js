require('dotenv').config()
module.exports = {
    service: "Gmail",
    auth: {
        user: process.env.TRANSPORT_USER,
        pass: process.env.TRANSPORT_PASS
    }
}