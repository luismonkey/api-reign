require('dotenv').config()
const mongoose = require('mongoose');

const url = process.env.NODE_ENV === 'production' ? process.env.DATA_BASE_PROD : process.env.DATA_BASE;
console.log('url db ', url);
module.exports = {
  connection: () => {
    mongoose.connect(url, {
      keepAlive: 300000,
      connectTimeoutMS: 30000,
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true,
    }).catch(error => console.log(error, 'ERROR CONNECTION'));
  }
};
